package app.suresh.info.realm.activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import app.suresh.info.realm.R;
import app.suresh.info.realm.adapters.BooksAdapter;
import app.suresh.info.realm.app.Prefs;
import app.suresh.info.realm.liveData.InboxListViewModel;
import app.suresh.info.realm.model.Book;
import app.suresh.info.realm.realm.RealmController;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {
    private InboxListViewModel inboxListViewModel;
    private BooksAdapter adapter;
    private LayoutInflater inflater;
    private FloatingActionButton fab;
    private RecyclerView recycler;
    private Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab =  findViewById(R.id.fab);
        recycler =  findViewById(R.id.recycler);


        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupRecycler();

        if (!Prefs.with(this).getPreLoad()) {
            setRealmData();
        } else {
            handler = new Handler(getMainLooper());
            handler.postDelayed(new Runnable() {
                public void run() {
//                    setBackgroundRealmData();
                    setBackgroundInsertData();
                    handler.postDelayed(this, 5000);
                }
            }, 100);
        }


    }

    private void setupRecycler() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);
        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        // create an empty adapter and add it to the recycler view
        adapter = new BooksAdapter(this, RealmController.with(MainActivity.this).getBooks());
        recycler.setAdapter(adapter);
    }

    private void setRealmData() {

        final ArrayList<Book> books = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            Book book = new Book();
            book.setId(System.currentTimeMillis() + i);
            book.setAuthor("Contact " + (System.currentTimeMillis() + i));
            book.setTitle("Android Development " + i);
            book.setImageUrl("https://1000logos.net/wp-content/uploads/2016/10/Android-Logo.png");
            books.add(book);
        }
        Log.e("books ", "setRealmData: " + books.size());
        // Persist your data easily
        RealmController.with(MainActivity.this).getRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(books);
                Log.e("books ", "execute: " + books.size());
            }
        });


        Prefs.with(this).setPreLoad(true);

    }

    private void setBackgroundRealmData() {


        final ArrayList<Book> books = new ArrayList<>();
        List<Book> bookRealmResults = RealmController.with(MainActivity.this).getBooksAsync();
        Log.e("books ", "bookRealmResults: " + bookRealmResults.size());
        for (int i = 0; i < bookRealmResults.size(); i++) {
            Book book = bookRealmResults.get(i);
            book.setAuthor("Contact " + System.currentTimeMillis()+40000);
            book.setTitle("Android Development " +1000);
            book.setImageUrl("https://1000logos.net/wp-content/uploads/2016/10/Android-Logo.png");
            books.add(book);
        }
        Log.e("books ", "setRealmData: " + books.size());
        // Persist your data easily
        //get realm instance
        RealmController.with(MainActivity.this).getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(books);
                Log.e("books ", "execute: " + books.size());
            }
        });
//
    }

    private void setBackgroundInsertData() {
        final ArrayList<Book> books = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Book book = new Book();
            book.setId(System.currentTimeMillis() + i);
            book.setAuthor("Contact " + (System.currentTimeMillis() + i));
            book.setTitle("Android Development " + 100+i);
            book.setImageUrl("https://1000logos.net/wp-content/uploads/2016/10/Android-Logo.png");
            books.add(book);
        }
        Log.e("books ", "setRealmData: " + books.size());
        // Persist your data easily
        //get realm instance
        RealmController.with(MainActivity.this).getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(books);
                Log.e("books ", "execute: " + books.size());
            }
        });
    }


}