package app.suresh.info.realm.realm;


import android.app.Activity;
import android.app.Application;

import java.util.List;

import androidx.fragment.app.Fragment;

import app.suresh.info.realm.model.Book;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {
        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }



    //find all objects in the Book.class
    public RealmResults<Book> getBooks() {
        return realm.where(Book.class).sort("id",Sort.DESCENDING).findAllAsync();
    }

    //query a single item with the given id
    public List<Book> getBooksAsync() {
        return realm.copyFromRealm(realm.where(Book.class).findAll());
    }


    //query example
    public RealmResults<Book> queryedBooks() {

        return realm.where(Book.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
