package app.suresh.info.realm.liveData;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import app.suresh.info.realm.model.Book;
import io.realm.Realm;

public class InboxListViewModel extends ViewModel {

    private final LiveData<List<Book>> inboxLiveData;
    Realm realm;

    public InboxListViewModel() {
//        realm = Realm.getInstance(SDKManager.realmConfig);
        inboxLiveData = new LiveRealmResults<>(realm.where(Book.class).findAllAsync(), realm);
    }

    public LiveData<List<Book>> getInboxLiveData() {
        return inboxLiveData;
    }

    @Override
    protected void onCleared() {
        realm.close();
        super.onCleared();
    }


}
